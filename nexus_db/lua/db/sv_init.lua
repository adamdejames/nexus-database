
AddCSLuaFile("cl_init.lua")

Player = FindMetaTable("Player")

cityrp.command.add("database", "b", 0, function(ply)
	if(ply:IsGovernment() or ply:IsAdmin()) then
		ply:ConCommand("nexus_db")
	else
		ply:Notify("You do not have access to this menu!", 1)
	end
end, "Commands", nil, "Opens the Nexus Database")

function Player:IsGovernment()
	return self:Team()== TEAM_GAL or self:Team()== TEAM_POLICECOMMANDER or self:Team()== TEAM_POLICEOFFICER or self:Team()== TEAM_GA or self:Team()== TEAM_PRESIDENT or self:Team()== TEAM_VP or self:Team()== TEAM_NEXUSDESK
end


util.AddNetworkString("nexusdatabase_warrant")
util.AddNetworkString("nexusdatabase_unwarrant")

net.Receive("nexusdatabase_unwarrant", function(len, ply)
	if(ply:IsGovernment()) then
		local target = cityrp.player.get(net.ReadString())
		if(target:IsPlayer()) then
			ply:ConCommand("say /unwarrant "..target:UserID())
		end
	end
end)

net.Receive("nexusdatabase_warrant", function(len, ply)
	if(ply:IsGovernment()) then
		local target = cityrp.player.get(net.ReadString())
		local class = net.ReadString()
		local reason = net.ReadString()
		if(target:IsPlayer()) then
			ply:ConCommand("say /warrant "..target:UserID().." "..class.." "..reason)
		end
	end
end)