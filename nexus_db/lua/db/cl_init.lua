--[[Clientside]]--


function nexus_db()
	local ply = LocalPlayer();
	if(!ply:Team()== TEAM_GAL or !ply:Team()== TEAM_POLICECOMMANDER or !ply:Team()== TEAM_POLICEOFFICER or !ply:Team()== TEAM_GA or !ply:Team()== TEAM_PRESIDENT or !ply:Team()== TEAM_VP or !ply:Team()== TEAM_NEXUSDESK) then return end
	local frame = vgui.Create("DFrame")
		frame:SetSize(600, 600)
		frame:Center()
		frame:SetTitle("Nexsus Database - Testing Alpha 1")
		frame:MakePopup()
	local sheet1 = vgui.Create("DPropertySheet", frame)
		sheet1:Dock(FILL)
	local warrants_panel = vgui.Create("DPanel", sheet1)
		local warrants_info = vgui.Create("DLabel", warrants_panel)
			warrants_info:SetTextColor(Color(0, 0, 0, 255))
			warrants_info:SetSize(300, 100)
			warrants_info:SetText("This area will display all active warrants.")
			warrants_info:SetPos(180, -20)
		local warrants_list = vgui.Create("DListView", warrants_panel)
			warrants_list:SetSize(350, 400)
			warrants_list:SetPos(115, 50)
			warrants_list:SetMultiSelect(false)
			warrants_list:AddColumn("Name")
			warrants_list:AddColumn("Type")
			warrants_list:AddColumn("Length")
			warrants_list:AddColumn("Reason")
			warrants_list:AddColumn("Wanted")
			for k, v in pairs(player.GetAll()) do
				if(v:GetNSVar("cityrp_Warranted")== "arrest") then
					warrants_list:AddLine(v:GetName(), v:GetNSVar("cityrp_Warranted"), "n/a", v:GetNSVar("cityrp_WarrantReason"), "Yes")
				else
					warrants_list:AddLine(v:GetName(), v:GetNSVar("cityrp_Warranted"), "n/a", v:GetNSVar("cityrp_WarrantReason"), "No")
				end
			end
		sheet1:AddSheet("Warrants", warrants_panel, "icon16/page_delete.png")
	local arrest_log = vgui.Create("DPanel", sheet1)
		sheet1:AddSheet("Arrests", arrest_log, "icon16/stop.png")
		local arrest_info = vgui.Create("DLabel", arrest_log)
			arrest_info:SetTextColor(Color(0, 0, 0, 255))
			arrest_info:SetSize(300, 100)
			arrest_info:SetText("This area displays all arrested players.")
			arrest_info:SetPos(190, -20)
		local arrest_list = vgui.Create("DListView", arrest_log)
			arrest_list:SetSize(350, 400)
			arrest_list:SetPos(115, 50)
			arrest_list:SetMultiSelect(false)
			arrest_list:AddColumn("Name")
			arrest_list:AddColumn("Length")

			for k, v in pairs(player.GetAll()) do
				if(v:GetNSVar("cityrp_Arrested")== true) then
					arrest_list:AddLine(v:GetName(), v._WarrantedTime)
				end
			end
	local police_cp = vgui.Create("DPanel", sheet1)
		if(ply:Team()== TEAM_POLICEOFFICER or ply:Team()== TEAM_POLICECOMMANDER) then
			sheet1:AddSheet("Police Control Panel", police_cp, "icon16/star.png")
		end
		local cp_info = vgui.Create("DLabel", police_cp)
			cp_info:SetTextColor(Color(0, 0, 0, 255))
			cp_info:SetSize(500, 100)
			cp_info:SetPos(100, -20)
			cp_info:SetText([[Here you can warrant players and unwarrant players. 
				(It is advised you get presidential permissions before unwarranting someone.)]])
		local warrantmenu = vgui.Create("DButton", police_cp)
			warrantmenu:SetSize(100, 30)
			warrantmenu:SetPos(100, 165)
			warrantmenu:SetText("Warrant a player")
			warrantmenu.DoClick = function()
				warrant_menu()
			end
		local unwarrantmenu = vgui.Create("DButton", police_cp)
			unwarrantmenu:SetSize(100, 30)
			unwarrantmenu:SetPos(100, 125)
			unwarrantmenu:SetText("Unwarrant a player")
			unwarrantmenu.DoClick = function()
				unwarrant_menu();
			end
end
concommand.Add("nexus_db", nexus_db)

function warrant_menu()
	local ply = LocalPlayer()
	local frame = vgui.Create("DFrame")
		frame:SetSize(500, 500)
		frame:Center()
		frame:SetTitle("Warrant menu")
		frame:MakePopup()
	local players = vgui.Create("DListView", frame)
		players:SetSize(350, 350)
		players:SetPos(75, 50)
		players:SetMultiSelect(false)
		players:AddColumn("Name")
		players:AddColumn("RP Name")
		for k, v in pairs(player.GetAll()) do
			players:AddLine(v:GetName(), v:RPName())
		end
	local reason = vgui.Create("DTextEntry", frame)
		reason:SetSize(100, 20)
		reason:SetPos(200, 415)
		reason:SetText("Reason")
	local warrantarrest = vgui.Create("DButton", frame)
		warrantarrest:SetSize(100, 20)
		warrantarrest:SetPos(75, 415)
		warrantarrest:SetText("Warrant for Arrest")
		warrantarrest.DoClick = function()
			net.Start("nexusdatabase_warrant")
				local selectedplayer = players:GetSelectedLine()
				net.WriteString(players:GetLine(selectedplayer):GetValue(1))
				net.WriteString("arrest")
				local reason = table.concat({reason:GetText()}, " ")
				net.WriteString(reason)
			net.SendToServer()
		end
	local warrantsearch = vgui.Create("DButton", frame)
		warrantsearch:SetSize(100, 20)
		warrantsearch:SetPos(75, 440)
		warrantsearch:SetText("Warrant for Search")
		warrantsearch.DoClick = function()
			net.Start("nexusdatabase_warrant")
				local selectedplayer = players:GetSelectedLine()
				print(players:GetLine(selectedplayer):GetValue(1))
				net.WriteString(players:GetLine(selectedplayer):GetValue(1))
				net.WriteString("search")
				local reason = table.concat({reason:GetText()}, " ")
				net.WriteString(reason)
			net.SendToServer()
		end
end

function unwarrant_menu()
	local ply = LocalPlayer()
	local frame = vgui.Create("DFrame")
		frame:SetSize(500, 500)
		frame:Center()
		frame:SetTitle("UnWarrant menu")
		frame:MakePopup()
	local players = vgui.Create("DListView", frame)
		players:SetSize(350, 350)
		players:SetPos(75, 50)
		players:SetMultiSelect(false)
		players:AddColumn("Name")
		players:AddColumn("RP Name")
		for k, v in pairs(player.GetAll()) do
			if(v:GetNSVar("cityrpWarranted")) then
				players:AddLine(v:GetName(), v:RPName())
			end
		end
	local warrantarrest = vgui.Create("DButton", frame)
		warrantarrest:SetSize(100, 20)
		warrantarrest:SetPos(75, 415)
		warrantarrest:SetText("UnWarrant")
		warrantarrest.DoClick = function()
			net.Start("nexusdatabase_unwarrant")
				local selectedplayer = players:GetSelectedLine()
				net.WriteString(players:GetLine(selectedplayer):GetValue(1))
			net.SendToServer()
		end
end